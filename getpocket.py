# -*- coding: utf-8 -*-
import urllib
import json
from urllib.request import *
from db import DB


class GetPocket:
    API_URL_ROOT = 'https://getpocket.com'
    #URL where GetPocket.com will direct browser to after the permission is authorized
    REDIRECT_URI = 'http://murphytalk.pythonanywhere.com'

    REQUEST_TOKEN = 'request_token'
    ACCESS_TOKEN = 'access_token'
    USER_NAME = 'username'
    CONSUMER_KEY = 'consumer_key'

    def __init__(self, consumer_key, getpocket_data, proxy_url=None):
        self.consumer_key = consumer_key
        self.data = getpocket_data

        if proxy_url is not None:
            proxy_handler = ProxyHandler({'http': proxy_url, 'https': proxy_url})
        else:
            proxy_handler = None

        self.opener = build_opener(HTTPRedirectHandler(),
                                   HTTPHandler(debuglevel=0),
                                   HTTPSHandler(debuglevel=0))

        if not proxy_handler is None:
            self.opener.add_handler(proxy_handler)

    def get_request_token_url(self):
        url = self.API_URL_ROOT + '/v3/oauth/request'
        step2 = self.opener.open(url,
                                 data=str.encode(urllib.parse.urlencode({self.CONSUMER_KEY: self.consumer_key,
                                                                         'redirect_uri': self.REDIRECT_URI})))
        self.data[self.REQUEST_TOKEN] = urllib.parse.parse_qs(step2.read().decode())['code'][0]
        #print(self.request_token)

        url = "{}?{}".format(self.API_URL_ROOT + '/auth/authorize',
                             urllib.parse.urlencode({'request_token': self.data[self.REQUEST_TOKEN],
                                                     'redirect_uri': self.REDIRECT_URI}))

        return url

    def get_pocket_access_token(self):
        url = self.API_URL_ROOT + '/v3/oauth/authorize'
        step5 = self.opener.open(url, data=str.encode(urllib.parse.urlencode({self.CONSUMER_KEY: self.consumer_key,
                                                                              'code': self.data[self.REQUEST_TOKEN]})))
        response = urllib.parse.parse_qs(step5.read().decode())
        self.data[self.ACCESS_TOKEN] = response[self.ACCESS_TOKEN][0]
        self.data[self.USER_NAME] = response[self.USER_NAME][0]

    def check_pocket_access_token(self):
        if not self.ACCESS_TOKEN in self.data:
            url = self.get_request_token_url()
            if url is not None:
                input("Please visit {} in browser to authorize and then press enter key to continue ...".format(url))
                self.get_pocket_access_token()
            else:
                return False
        return True

    def get_bookmarks(self):
        url = self.API_URL_ROOT + '/v3/get'
        remote = self.opener.open(url,
                                  data=str.encode(urllib.parse.urlencode({self.CONSUMER_KEY: self.consumer_key,
                                                                          self.ACCESS_TOKEN: self.data[self.ACCESS_TOKEN],
                                                                          'sort': 'newest',
                                                                          'detailType': 'complete',
                                                                          })))
        j = json.loads(remote.read().decode(), 'utf-8')

        bookmarks = {}
        for k,v in j['list'].items():
            bkmk = {}
            bkmk[DB.ID] = k
            bkmk[DB.TITLE] = v['resolved_title'] if 'resolved_title' in v and len(v['resolved_title']) > 0 else v['given_title']
            bkmk[DB.URL] = v['resolved_url'] if 'resolved_url' in v and len(v['resolved_url']) > 0 else v['given_url']
            #bkmk[DB.TAGS] = [x for x in v['tags'].keys()] if 'tags' in v else None
            bkmk[DB.TAGS] = v['tags'] if 'tags' in v else None
            bkmk[DB.TIMESTAMP] = v['time_updated']
            bookmarks[k] = bkmk
        return bookmarks












