# -*- coding: utf-8 -*-
from getpocket import *
from db import *
import configparser
import sys

if __name__ == '__main__':
    """
    accept one cmd lind parameter : config file
    default to sync-bookmarks.ini

    ini file:

    [General]
    db=path-to-db-file
    proxy=proxy-url

    [GetPocket]
    consumer_key=
    """

    config = configparser.RawConfigParser(allow_no_value=True)
    config.read(sys.argv[1] if len(sys.argv) == 2 else 'sync-bookmarks.ini')

    #see if proxy is specified in .ini
    proxy = None
    try:
        proxy = config.get('General', 'proxy')
    except configparser.NoSectionError:
        pass

    db = DB(config.get('General', 'db'))
    db.load_db()

    pocket_data = db.data[db.GETPOCKET]
    pocket = GetPocket(config.get('GetPocket', 'consumer_key'), pocket_data, proxy)

    if not pocket.check_pocket_access_token():
        print("Failed to retrieve GetPocket access token!")
    else:
        pocket_data[db.BOOKMARKS] = pocket.get_bookmarks()
        db.save_db()