# -*- coding: utf-8 -*-
import os.path
import json
#import codecs


class DB:
    DB_ENCODING = 'utf-8'
    GETPOCKET = 'pocket'
    DELICIOUS = 'delicious'
    BOOKMARKS = 'bookmarks'

    TITLE = 'Title'
    ID = 'id'
    URL = 'url'
    TAGS = 'tags'
    TIMESTAMP = 'timestamp'

    data = None

    def __init__(self, db_path):
        self.db_path = db_path

    def load_db(self):
        self.data = None

        if os.path.isfile(self.db_path):
            #f = codecs.open(self.db_path, 'r', self.DB_ENCODING)
            f = open(self.db_path, 'r')
            try:
                self.data = json.loads(f.read(), self.DB_ENCODING)
            except ValueError:
                pass
            f.close()

        if self.data is None:
            self.data = {self.GETPOCKET: {}, self.DELICIOUS: {}}

    def save_db(self):
        #f = codecs.open(self.db_path, 'w', self.DB_ENCODING)
        f = open(self.db_path, 'w')
        json.dump(self.data, f, indent=4, separators=(',', ': '))
        f.close()


